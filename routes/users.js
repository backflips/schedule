var express = require('express');


var userRouter = function(db) {
	
	function checkUserID(req, res, next) {
		var userID = parseInt(req.params.id);
		
		db.users.checkId(userID)
			.then(function(data) {
				if(!data) {
					res.status(400)
						.json({
							status: 'error',
							message: `UserID '${userID}' does not exist.`
						});
				}
				else {
					return next();
				}
			})
			.catch(function(err) {
				return next(err);
			});
	}
	
	function checkForValidFields(req, res, next) {
		var fields = new Array;
		
		db.users.getFields()
			.then(function(data) {
				for(var f in data) {
					fields.push(data[f].column_name);
				}
				for(var t in req.body){
					if(!(fields.indexOf(t) >=0)){
						return res.status(400)
						.json({
							status: 'error',
							message: `'${t}' is not a field.`
						});
						
					}
				}
				return next()
			})
			.catch(function(err) {
				console.log("here2")
				throw err;
			})
	}
	
	function checkRequiredFields(req, res, next) {
		var required = new Array;
		
		db.users.getRequiredFields()
			.then(function(data) {
				for(var f in data) {
					required.push(data[f].column_name);
				}
				console.log(required)
				for(var r in required) {
					console.log(required[r]);
					if(req.body[required[r]] == null) {
						return res.status(400)
						.json({
							status: 'error',
							message: `'${required[r]}' is required.`
						});
					}
				}
				return next();
				
			})
			.catch(function(err) {
				console.log(err)
				throw err;
			})
	}
	
	function checkRequiredPostData(req, res, next) {
		var payload = req.body;
		var fields = ['firstname', 'lastname'];
		var required = ['firstname', 'lastname'];
		var missing = [];
		
		required.forEach(function(element) {
			if(!(element in req.body)) {
				missing.push(element);
			}
		});
		
		if(missing.length > 0) {
			var message = missing.join() + " is required"
			res.status(400)
				.json({
					status: 'error',
					message: message
				});
		}
		else {
			next();
		}
		
	}
		
	
	
    var router = express.Router();
    
    router.route('/')
        .get(function(req, res, next) {
           db.any('SELECT * FROM users')
                .then(function(data) {
                    res.status(200)
                        .json({
                            status: 'success',
                            data:   data,
                            message: 'Queried all users'
                        }); 
                })
                .catch(function(err) {
                    console.log(err)
                    return next(err); 
                });            
        })
        .post(checkRequiredPostData, function(req, res, next) {
            db.none('INSERT INTO users (firstname, lastname)' +
           'VALUES(${firstname}, ${lastname})', req.body) 
                .then(function() {
                    res.status(200)
                        .json({
                            status: 'success',
                            message: 'Inserted a User'
                        });
                })
                .catch(function(err) {
                    return next(err);
                })
        });
        
		
	router.route('/test')
		.get(function(req, res, next) {
			for(var i = 0; i < 100; i++) {
				console.log(i.toString())
				db.oneOrNone('INSERT INTO users (username, password) VALUES ($1, $2)', [i,i])
				.catch(function(err) {
					console.log(err);
				});
			}
			res.send('good');
		})
		
	router.route('/test2')
		.get(function(req, res, next) {
			var hash = null;
			db.users.comparePassword(1, "test")
			.then(function(data) {
				console.log(data);
			})
			.catch(function(err) {
				console.log(err)
			});
			
			
			
			
			res.send("test");
		})
		.post(checkForValidFields, checkRequiredFields ,function(req, res, next) {
			res.send("test");
		})
		
	router.route('/test3')
		.get(function(req, res, next) {
			db.users.hashPassword("test")
				.then(function(hash) {
					console.log(hash);
					res.send(hash);
				})
		});
        
    router.route('/:id')
        .get(checkUserID, function(req, res, next) {
            var id = req.params.id;
            /*db.one('SELECT * FROM users WHERE id = $1', id)
                .then(function(data) {
					res.status(200)
						.json({
							status: 'success',
							data:   data,
							message: `Found UserID '${id}'`
						});
                })
                .catch(function(err) {
                    return next(err); 
                });
				*/
			db.users.findById(id)
				.then(function(data) {
					res.send(data);
				})
				.catch(function(err) {
					console.log(err);
				});
        });
		
    return router;
};

module.exports = {
    users: userRouter
}