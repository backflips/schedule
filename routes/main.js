var express = require('express');

var mainRouter = function(db) {
    var router = express.Router();
    var users = require('./users').users(db);
    
    router.route('/')
        .get(function(req, res) {
            res.status(200)
                .json({
                    status: 'success',
                    data: null,
                    message: 'Welcome to the schedule'         
                });
        });
        
        
    router.use('/users', users);
    
    return router;
};

module.exports = {
    mainRouter: mainRouter
}