DROP DATABASE IF EXISTS schedule;
CREATE DATABASE schedule;

\c schedule;

CREATE TABLE users (
    users_ID SERIAL PRIMARY KEY,
	username VARCHAR UNIQUE NOT NULL,
	password VARCHAR NOT NULL,
	email VARCHAR UNIQUE
);

CREATE TABLE userProfile (
	userProfile_ID SERIAL PRIMARY KEY,
	firstname VARCHAR,
	lastname VARCHAR,
	user_id INTEGER NOT NULL REFERENCES users(users_ID)
);

INSERT INTO users (username, password, email)
    VALUES ('mhood', '$2a$10$7x9qphyUxi.54nuRDxCL3ekqAwmexFdLNQv37zjrUmsUR6DVSUf.u', 'mhood@outlook.com');
	
INSERT INTO userProfile(firstname, lastname, user_id)
	VALUES('Mike', 'Hood', 1);