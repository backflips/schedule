var users = require('../models/users.js');

var options = {
	extend: function() {
		this.users = users.userFunctions(this);
	}
};

var pgp = require('pg-promise')(options);

var connectionString = 'postgres://Mike:Mn6y88kc@localhost:5432/schedule'
var db = pgp(connectionString);

module.exports = {
    db: db
}