var bcrypt = require('bcrypt-nodejs');

var userFunctions = function(db) {
	
	var findAll = function() {
		return db.oneOrNone("SELECT * FROM users");	
	}
	
	var checkId = function(id) {
		return db.oneOrNone('SELECT users_ID FROM users WHERE users_ID = $1', id);
	}
	
	var findById = function(id) {
		return db.one('SELECT * FROM users WHERE users_ID = $1', id);
	}
	
	var getFields = function() {
		return db.any("SELECT column_name FROM information_schema.columns WHERE table_name='users' AND column_name<>'users_id'");
	}
	
	var getRequiredFields = function() {
		return db.any("SELECT column_name FROM information_schema.columns WHERE table_name='users' and is_nullable='NO' AND column_name<>'users_id'")
	}
	
	var comparePassword = function(id, password) {
		var promise = new Promise(function(resolve, reject) {
			db.one('SELECT password FROM users WHERE users_ID = $1', id)
			.then(function(data) {
				resolve(bcrypt.compareSync(password,data.password));
			})
			.catch(function(err) {
				reject(err);
			});
		});
		
		return promise;
	}
	
	var hashPassword = function(password) {
		var promise = new Promise(function(resolve, reject) {
			bcrypt.hash(password, null, null, function(err, hash) {
				if(err) reject(err);
				
				resolve(hash);
			});
		});
		
		return promise;
	}
	
	return {
		findAll				: findAll,
		findById			: findById,
		checkId				: checkId,
		getFields			: getFields,
		getRequiredFields	: getRequiredFields,
		comparePassword		: comparePassword,
		hashPassword		: hashPassword
	}
}

module.exports = {
	userFunctions : userFunctions
}