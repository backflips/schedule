var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var db = require('./config/database').db;


var app = express();


app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', require('./routes/main').mainRouter(db));


/*app.get('/', function(req, res) {
	db.any('select * from users')
        .then(function(data) {
            res.status(200)
                .json({
                    status: 'success',
                    data:	data,
                    message: 'Got the items'
                });
        })
        .catch(function(err) {
            return next(err);
        });
});*/




var port = process.env.PORT || 3000;

app.listen(port);
console.log("Server started on port " + port);